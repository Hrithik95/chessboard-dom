let chessBoard = document.getElementById('chessboard');
let rowElement = document.getElementById('row');
let grid = [];

let rows = 8;
let cols = 8;

for (let rowIndex = 0; rowIndex < rows; rowIndex++) {
    let row = [];
    for (let colIndex = 0; colIndex < cols; colIndex++) {
        let element = document.createElement('div');
        element.style.border = "2px solid red";
        element.style.height = "100px";
        element.style.width = "100px";
        element.classList.add('element');
        element.classList.add(`element-${colIndex}${rowIndex}`);
        element.addEventListener('click', () => {
            let allElements = document.querySelectorAll('.red');
            console.log(allElements);
            for (let index = 0; index < allElements.length; index++) {
                allElements[index].classList.remove('red');
            }
            moveLeftUp(colIndex, rowIndex);
            moveRightUp(colIndex, rowIndex);
            moveLeftDown(colIndex, rowIndex);
            moveRightDown(colIndex, rowIndex);
        })
        if (rowIndex % 2 == 0) {
            if (colIndex % 2 == 0) {
                element.style.backgroundColor = 'white';
            } else {
                element.style.backgroundColor = 'black';
            }
        } else {
            if (colIndex % 2 == 0) {
                element.style.backgroundColor = 'black';
            } else {
                element.style.backgroundColor = 'white';
            }
        }
        row.push(element);
    }
    grid.push(row);
}

grid.map((row, rowIndex) => {
    let rowEle = document.createElement('div');
    row.map((col, colIndex) => {
        rowEle.append(col);
    });
    chessBoard.append(rowEle);
});

function moveLeftUp(colIndexValue, rowIndexValue) {
    while (colIndexValue >= 0 && rowIndexValue >= 0) {
        let element = document.querySelector(`.element-${colIndexValue}${rowIndexValue}`);
        element.classList.add('red');
        colIndexValue--;
        rowIndexValue--;
    }
}

function moveRightUp(colIndexValue, rowIndexValue) {
    while (colIndexValue < 8 && rowIndexValue >= 0) {
        let element = document.querySelector(`.element-${colIndexValue}${rowIndexValue}`);
        element.classList.add('red');
        colIndexValue++;
        rowIndexValue--;
    }
}

function moveLeftDown(colIndexValue, rowIndexValue) {
    while (colIndexValue >= 0 && rowIndexValue < 8) {
        let element = document.querySelector(`.element-${colIndexValue}${rowIndexValue}`);
        element.classList.add('red');
        colIndexValue--;
        rowIndexValue++;
    }
}

function moveRightDown(colIndexValue, rowIndexValue) {
    while (colIndexValue < 8 && rowIndexValue < 8) {
        let element = document.querySelector(`.element-${colIndexValue}${rowIndexValue}`);
        element.classList.add('red');
        colIndexValue++;
        rowIndexValue++;
    }
}


